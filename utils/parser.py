from typing import Iterator, List

from models.exceptions import DataNotFound
from models.pilot import Pilot
from models.route_point import RoutePoint
from services.vatsim import get_data


def route_points(route: str) -> Iterator[RoutePoint]:
    for point in route.split(' '):
        if point.upper() == 'DCT' or point.upper() == "T25":
            continue
        yield RoutePoint(identifier=point)


def extend_airways(flight_plan_points: List[str], airways: dict) -> List[str]:
    result: List[str] = flight_plan_points
    for airway_key in airways.keys():
        if airway_key in flight_plan_points:
            result: List[str] = process_airway(airway_key, result, airways)
    return result


def process_airway(airway_job: str, memoization: List[str], airways: dict) -> List[str]:
    airway_index: int = memoization.index(airway_job)

    initial_point: str = memoization[airway_index - 1]
    final_point: str = memoization[airway_index + 1]

    airway_route: List = airways[airway_job]
    airway_initial_index: int = airway_route.index(initial_point)
    airway_final_index: int = airway_route.index(final_point)

    if airway_initial_index > airway_final_index:
        airway_route.reverse()
        airway_initial_index: int = airway_route.index(initial_point)
        airway_final_index: int = airway_route.index(final_point)

    to_extend: List[str] = airway_route[airway_initial_index + 1:airway_final_index]
    return memoization[:airway_index] + to_extend + memoization[airway_index + 1:]


def online_pilots() -> Iterator[Pilot]:
    for pilot in get_data()["pilots"]:
        yield Pilot(**pilot)


def online_pilot(cid: int) -> Pilot:
    for pilot in online_pilots():
        if int(pilot.cid) == int(cid):
            return pilot
    raise DataNotFound(__name__, f"{cid} not found on vatsim data")


def flight_plan_route(callsign: str) -> List[str]:
    AIRWAYS = {
        "T16": ['OMOKO', 'GONAN', 'EKROL', 'NAVIX', 'IDREL', 'EXODO', 'SNT'],
        "T13": ['IDREL', 'RUPEP', 'MANOX', 'LUPOV', 'NILAV', 'OMOKO']
    }

    pilot = online_pilot(callsign)
    try:
        flight_plan_points = [point.identifier for point in route_points(pilot.flight_plan.route)]
    except AttributeError as error:
        raise DataNotFound(__name__, error)
    if airway_in_flight_plan(flight_plan_points, AIRWAYS):
        return extend_airways(flight_plan_points, AIRWAYS)
    return flight_plan_points


def airway_in_flight_plan(flight_plan_points: List[str], airways: dict) -> bool:
    return True if airways.keys() in flight_plan_points else False

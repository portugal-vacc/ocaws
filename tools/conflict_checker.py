from datetime import datetime, timedelta

from models.position_report import PositionReport
from models.route_point import RoutePoint


def conflict(position_report_1: PositionReport, position_report_2: PositionReport) -> bool:
    """Returns true if there is conflict between the two position reports. Returns false otherwise.
    It checks if the two position reports are converging in the same waypoint, if the position reports are
    at the same altitude and if they have the required longitudinal separation based on their Mach number"""
    are_to_same_point: bool = to_same_point(position_report_1.route_point_next_position,
                                            position_report_2.route_point_next_position)
    are_at_same_level: bool = at_same_level(position_report_1.flight_level, position_report_2.flight_level)
    are_fans_a_separated = fans_a_separation(position_report_1.datetime_next_position_time,
                                             position_report_2.datetime_next_position_time)
    are_mach_number_technique_separated = mach_number_technique_separation(
        position_report_1.datetime_next_position_time, position_report_1.mach_number,
        position_report_2.datetime_next_position_time, position_report_2.mach_number)

    print("to_same_point", to_same_point, "at_same_level", at_same_level, "fans_a_separation", fans_a_separation,
          "mach_number_technique_separation", mach_number_technique_separation)

    if are_to_same_point and are_at_same_level:
        if position_report_1.fans_a and position_report_2.fans_a:
            if not are_fans_a_separated:
                return True
        elif not are_mach_number_technique_separated:
            return True

    return False


def separation_minutes(next_position_time_1: datetime, next_position_time_2: datetime) -> int:
    """Returns the separation in minutes between next_position_time_1 and next_position_time_2."""
    time_delta: timedelta = (next_position_time_1 - next_position_time_2)
    return abs(round(time_delta.total_seconds() / 60))


def at_same_level(flight_level_1: int, flight_level_2: int) -> bool:
    """Returns true if flight_level_1 is the same as flight_level_2, meaning that the two position reports
    are at the same altitude. Returns false otherwise"""
    return True if flight_level_1 == flight_level_2 else False


def to_same_point(next_position_1: RoutePoint, next_position_2: RoutePoint) -> bool:
    """Returns true if the two position reports are converging in the same waypoint, by comparing the next_position.
     Returns false otherwise."""
    return True if next_position_1.identifier == next_position_2.identifier else False


def fans_a_separation(next_position_time_1: datetime, next_position_time_2: datetime) -> bool:
    """Returns true if at least 5 minute separation exists between FANS-A aircraft. Returns false otherwise.
    It does so by checking the two time estimates of the two aircraft."""
    time_separation: int = separation_minutes(next_position_time_1, next_position_time_2)
    return True if time_separation >= 5 else False


def mach_number_technique_separation(next_position_time_1: datetime, mach_1: int,
                                     next_position_time_2: datetime, mach_2: int) -> bool:
    """Returns true if the required mach number technique (mnt) separation exists. Returns false otherwise.
    It does so by checking the mach number of the twu reports, the next_position_time of the two reports and
    comparing against the MNT table

    We ignore negative mach_difference values"""
    mach_difference: int = 0
    time_separation: int = separation_minutes(next_position_time_1, next_position_time_2)
    if next_position_time_1 < next_position_time_2:
        """Plane 1 arrives first"""
        mach_difference = mach_1 - mach_2
    if next_position_time_1 > next_position_time_2:
        """Plane 2 arrives first"""
        mach_difference = mach_2 - mach_1

    if time_separation >= 10:
        return True

    if time_separation == 9 and mach_difference >= 2:
        return True

    if time_separation == 8 and mach_difference >= 3:
        return True

    if time_separation == 7 and mach_difference >= 4:
        return True

    if time_separation == 6 and mach_difference >= 5:
        return True

    if time_separation == 5 and mach_difference >= 6:
        return True

    return False

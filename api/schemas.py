from ninja import Schema


class ControllerInSchema(Schema):
    cid: int
    callsign: str


class ControllerOutSchema(Schema):
    cid: int
    callsign: str


class PilotInSchema(Schema):
    cid: int
    callsign: str


class PilotOutSchema(Schema):
    cid: int
    callsign: str
    assumed_by: ControllerOutSchema


class ReportInSchema(Schema):
    belongs_to_cid: int
    created_by_cid: int
    current_position: str
    current_position_time: str
    flight_level: int
    mach_number: int
    next_position: str
    next_position_time: str
    thereafter_position: str
    selcal: str
    fans_a: bool


class ReportOutSchema(Schema):
    belongs_to: PilotOutSchema
    created_by: ControllerOutSchema
    current_position: str
    current_position_time: str
    flight_level: int
    mach_number: int
    next_position: str
    next_position_time: str
    thereafter_position: str
    selcal: str
    fans_a: bool

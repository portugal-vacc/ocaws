from django.contrib import admin

# Register your models here.
from api.models import Controller, Pilot, Report

admin.site.register(Controller)
admin.site.register(Pilot)
admin.site.register(Report)

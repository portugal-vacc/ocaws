from typing import List, Dict

from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import get_object_or_404, get_list_or_404, redirect
from ninja import NinjaAPI, Form

from api.models import Report, Pilot, Controller
from api.schemas import ReportOutSchema, PilotOutSchema, ControllerOutSchema
from models.exceptions import DataNotFound
from utils.parser import online_pilots, route_points, online_pilot

my_api = NinjaAPI()
app_name = 'api'


@my_api.get("/live/pilots", tags=["live"])
def get_live_data_all_pilots(request):
    return [pilot for pilot in online_pilots()]


@my_api.get("/live/pilots/{cid}", tags=["live"])
def get_live_data_pilot(request, cid: int):
    try:
        return online_pilot(cid)
    except DataNotFound:
        raise Http404


@my_api.get("/live/pilots/{cid}/route", tags=["live"], response=List[str])
def get_pilot_route(request, cid: int):
    try:
        pilot_live_data = online_pilot(cid)
    except DataNotFound:
        raise Http404
    return [point.identifier for point in route_points(pilot_live_data.flight_plan.route)]


@my_api.get("/controllers", tags=["controllers"], response=List[ControllerOutSchema])
def get_all_controllers(request):
    return Controller.objects.all()


@my_api.get("/controllers/{cid}", tags=["controllers"], response=ControllerOutSchema)
def get_controller(request, cid: int):
    return get_object_or_404(Controller, cid=cid)


@my_api.get("/pilots", tags=["pilots"], response=List[PilotOutSchema])
def get_all_pilots(request):
    return Pilot.objects.all()


@my_api.get("/pilots/{cid}", tags=["pilots"], response=PilotOutSchema)
def get_pilot(request, cid: int):
    return get_object_or_404(Pilot, cid=cid)


@my_api.post("/pilots/{cid}", tags=["pilots"])
def post_pilot(request, cid: int, controller_cid: int):
    try:
        pilot_live_data = online_pilot(cid)
    except DataNotFound:
        raise Http404
    controller = get_object_or_404(Controller, cid=controller_cid)
    try:
        Pilot.objects.create(assumed_by=controller, cid=pilot_live_data.cid, callsign=pilot_live_data.callsign)
    except IntegrityError:
        return my_api.create_response(
            request,
            {"message": "Pilot is already created"},
            status=403)
    return {"success": True}


@my_api.delete("/pilots/{cid}", tags=["pilots"], response=Dict)
def delete_pilot(request, cid: int):
    pilot = get_object_or_404(Pilot, cid=cid)
    pilot.delete()
    return {"success": True}


@my_api.get("/reports", tags=["reports"], response=List[ReportOutSchema])
def get_all_reports(request):
    return Report.objects.all()


@my_api.post("/reports/{cid}", tags=["reports"])
def post_report(request, cid: int, controller_cid: int,
                current_position: str = Form(...), current_position_time: str = Form(...),
                flight_level: int = Form(...), mach_number: int = Form(...),
                next_position: str = Form(...), next_position_time: str = Form(...),
                thereafter_position: str = Form(...),
                selcal: str = Form(...), fansa: bool = Form(default=False)):
    pilot = get_object_or_404(Pilot, cid=cid)
    controller = get_object_or_404(Controller, cid=controller_cid)
    if pilot.assumed_by != controller:
        return my_api.create_response(
            request,
            {"message": "You don't have this aircraft assumed"},
            status=403)
    # TODO: Test conflict before database entry
    Report.objects.create(belongs_to=pilot, created_by=controller, current_position=current_position,
                          current_position_time=current_position_time, flight_level=flight_level,
                          mach_number=mach_number, next_position=next_position, next_position_time=next_position_time,
                          thereafter_position=thereafter_position, selcal=selcal, fans_a=fansa)
    return redirect(f'/{cid}')


@my_api.get("/reports/{cid}", tags=["reports"], response=List[ReportOutSchema])
def get_reports_pilot(request, cid: int):
    pilot = get_object_or_404(Pilot, cid=cid)
    return get_list_or_404(Report, belongs_to=pilot)


@my_api.get("/reports/{cid}/{id}", tags=["reports"], response=ReportOutSchema)
def get_report_pilot(request, cid: int, id: int):
    pilot = get_object_or_404(Pilot, cid=cid)
    reports = get_list_or_404(Report, belongs_to=pilot)
    try:
        return reports[id]
    except IndexError:
        raise Http404


@my_api.delete("/reports/{cid}/{id}", tags=["reports"], response=Dict)
def delete_pilot_report(request, cid: int, id: int):
    pilot = get_object_or_404(Pilot, cid=cid)
    reports = get_list_or_404(Report, belongs_to=pilot)
    try:
        report = reports[id]
    except IndexError:
        raise Http404
    report.delete()
    return {"success": True}

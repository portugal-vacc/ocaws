from django.db import models

# Create your models here.
from models.atc import ATC


class Report(models.Model):
    belongs_to = models.ForeignKey('Pilot', on_delete=models.CASCADE)
    created_by = models.ForeignKey('Controller', on_delete=models.CASCADE)
    current_position = models.CharField(max_length=30)
    current_position_time = models.CharField(max_length=30)
    flight_level = models.PositiveSmallIntegerField()
    mach_number = models.PositiveSmallIntegerField()
    next_position = models.CharField(max_length=30)
    next_position_time = models.CharField(max_length=30)
    thereafter_position = models.CharField(max_length=30)
    selcal = models.CharField(max_length=4)
    fans_a = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.belongs_to.callsign} - {self.current_position} -> {self.next_position}"


class Controller(models.Model):
    cid = models.CharField(max_length=30, unique=True)
    callsign = models.CharField(max_length=30)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.callsign} - {self.cid}"

    def live_data(self) -> ATC:
        pass

    @property
    def facility(self) -> str:
        return "LPPO_FSS"


class Pilot(models.Model):
    cid = models.CharField(max_length=30, unique=True)
    callsign = models.CharField(max_length=30)
    assumed_by = models.ForeignKey('Controller', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.callsign} - {self.cid}"

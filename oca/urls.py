from django.urls import path

from oca import views

app_name = 'oca'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:pilot_cid>/', views.pilot, name='pilot'),
    path('<int:pilot_cid>/report', views.report, name='report'),
    path('<int:pilot_cid>/clearence', views.clearance, name='clearance'),
]

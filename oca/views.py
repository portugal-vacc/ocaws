from django.shortcuts import render


def index(request):
    return render(request, 'layout.html', {"controller_cid": 1438868})


def pilot(request, pilot_cid: int):
    return render(request, 'pilot-profile.html', {"pilot_cid": pilot_cid, "controller_cid": 1438868})


def report(request, pilot_cid: int):
    return render(request, 'position-report.html', {"pilot_cid": pilot_cid, "controller_cid": 1438868})


def clearance(request, pilot_cid: int):
    return render(request, 'oceanic-clearance.html', {"pilot_cid": pilot_cid, "controller_cid": 1438868})

import requests

from cachetools import TTLCache, cached


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_data() -> dict:
    r = requests.get("https://data.vatsim.net/v3/vatsim-data.json")
    r.raise_for_status()
    return r.json()

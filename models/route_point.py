import re
from dataclasses import dataclass

from pydantic import BaseModel, validator


@dataclass
class RegexPoint:
    latitude: str
    horizontal_hemisphere: str
    longitude: str
    flag_position: str
    short_longitude: str
    vertical_hemisphere: str

    def __post_init__(self):
        assert 0 <= int(self.latitude) <= 90
        assert 0 <= int(self.longitude) <= 180

    def quadrant(self):
        if self.horizontal_hemisphere == 'N' and self.vertical_hemisphere == 'W':
            return 'N'
        if self.horizontal_hemisphere == 'N' and self.vertical_hemisphere == 'E':
            return 'E'
        if self.horizontal_hemisphere == 'S' and self.vertical_hemisphere == 'W':
            return 'W'
        if self.horizontal_hemisphere == 'S' and self.vertical_hemisphere == 'E':
            return 'S'


class RoutePoint(BaseModel):
    identifier: str = ''

    @validator('identifier')
    def format(cls, v):
        v = v.replace('+', '')
        v = v.upper()
        v = v.split('/')[0]
        return v.strip()

    @validator('identifier')
    def convert_to_arinc(cls, v):
        match = re.match(r"(\d{2})([NS])((\d)(\d{2}))([WE])", v)
        if not match:
            return v
        try:
            point = RegexPoint(*match.groups())
        except AssertionError:
            return v
        if not int(point.flag_position):
            return f"{point.latitude}{point.short_longitude}{point.quadrant()}"
        return f"{point.latitude}{point.quadrant()}{point.short_longitude}"

from typing import Optional

from pydantic import BaseModel, validator

from models.flight_plan import FlightPlan


class Pilot(BaseModel):
    cid: int
    name: str
    callsign: str
    server: str
    pilot_rating: int
    latitude: float
    longitude: float
    altitude: int
    groundspeed: int
    transponder: int
    heading: int
    qnh_i_hg: float
    qnh_mb: int
    flight_plan: Optional[FlightPlan]
    logon_time: str
    last_updated: str

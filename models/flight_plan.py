from pydantic import BaseModel


class FlightPlan(BaseModel):
    flight_rules: str
    aircraft: str
    aircraft_faa: str
    aircraft_short: str
    departure: str
    arrival: str
    alternate: str
    cruise_tas: int
    altitude: str
    deptime: int
    enroute_time: int
    fuel_time: int
    remarks: str
    route: str
    revision_id: int
    assigned_transponder: str

    def selcal(self) -> str:
        """Returns a string with the SELCAL in uppercase from remarks"""
        pass

from datetime import datetime

from pydantic.main import BaseModel

from models.route_point import RoutePoint


class PositionReport(BaseModel):
    call_sign: str
    current_position: str
    current_position_time: str
    flight_level: int = 300
    mach_number: int = 82
    next_position: str
    next_position_time: str
    thereafter_position: str
    selcal: str = 'ABCD'
    fans_a: bool = False

    @property
    def route_point_current_position(self) -> RoutePoint:
        return RoutePoint(self.current_position)

    @property
    def route_point_next_position(self) -> RoutePoint:
        return RoutePoint(self.next_position)

    @property
    def datetime_current_position_time(self) -> datetime:
        """
        current_position_time is a string with format HH:MM (example 12:12)
        :return: default day, month, year, second correspondent hour and minute
        :rtype: datetime
        """
        return datetime.strptime(self.current_position_time, "%H:%M")

    @property
    def datetime_next_position_time(self) -> datetime:
        """
        next_position_time is a string with format HH:MM (example 12:12)
        :return: default day, month, year, second correspondent hour and minute
        :rtype: datetime
        """
        next_position_time = datetime.strptime(self.next_position_time, "%H:%M")
        if next_position_time < self.datetime_current_position_time:
            return next_position_time.replace(day=2)
        return next_position_time

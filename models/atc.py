from typing import Optional, List

from pydantic import BaseModel, validator


class ATC(BaseModel):
    cid: int
    name: str
    callsign: str
    frequency: float
    facility: int
    rating: int
    server: str
    visual_range: int
    text_atis: Optional[List]
    last_updated: str
    logon_time: str

    @validator('text_atis')
    def text_atis_exists(cls, v):
        if v is None:
            return []
        return v

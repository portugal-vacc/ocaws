class ApiError(Exception):
    def __init__(self, message: str):
        super().__init__(f'[API] {str(message)}')


class DataNotFound(ApiError):
    def __init__(self, location: str, error: str):
        super().__init__(f'Could not find requested data\nError: {str(error)}\nIn: {str(location)}')
